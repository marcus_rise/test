module.exports = {
    preset: "ts-jest",
    testMatch: ["**/*.spec.{ts,js}"]
}
