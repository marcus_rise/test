import {arrayFromLetter} from "./array-from-letter.function";

describe("arrayFromLetter", () => {
    test("A", (done) => {
        let res: string[] = [];

        arrayFromLetter("A").subscribe({
            next: (val) => {
              res.push(val);
            },
            complete: () => {
                expect(res).toHaveLength(2);
                expect(res[0]).toEqual("A1");
                expect(res[1]).toEqual("A2");

                done();
            }
        })
    });
})
