import {alphabetGenerator} from "./alphabet-generator.function";

alphabetGenerator("A", "B", "C").subscribe({
    next: console.log,
    complete: () => console.log("end"),
})
