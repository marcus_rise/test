import {alphabetGenerator} from "./alphabet-generator.function";

describe("alphabetGenerator", () => {
    test(`"A", "B", "C"`, (done) => {
        let res: string[] = [];

        alphabetGenerator("A", "B", "C").subscribe({
            next: (value) => {
                res.push(value);
            },
            complete: () => {
                expect(res).toHaveLength(6);
                expect(res.includes("A1")).toBeTruthy();
                expect(res.includes("A2")).toBeTruthy();
                expect(res.includes("B1")).toBeTruthy();
                expect(res.includes("B2")).toBeTruthy();
                expect(res.includes("C1")).toBeTruthy();
                expect(res.includes("C2")).toBeTruthy();
            }
        })
    })
})
