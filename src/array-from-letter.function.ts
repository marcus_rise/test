import {Observable, of} from "rxjs";
import {delay, mergeMap} from "rxjs/operators";

export const arrayFromLetter = (char: string): Observable<string> => {
    const source = of(1, 2);

    return source.pipe(
        mergeMap((value) => of(`${char}${value}`).pipe(delay(1000))),
    );
}
