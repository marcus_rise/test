import {Observable, of} from "rxjs";
import {mergeMap} from "rxjs/operators";
import {arrayFromLetter} from "./array-from-letter.function";

export const alphabetGenerator = (...alph: string[]): Observable<string> => {
    const source = of(...alph);

    return source.pipe(
        mergeMap((value) => arrayFromLetter(value)),
    );
}
